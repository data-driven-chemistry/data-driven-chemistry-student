# Session 2 Loops and Pandas

## This is the second session of data driven chemistry

Aims
----

In this session we will be looking at how to automate processes through understanding `loops`. 
We will encounter:
- `for loops`
- `if loops`

and understand how they work alongside:
- else
- elif
- break.


The second aim of this session is introduce pandas.
Pandas is a high-level data manipulation tool built on the Numpy package and its key data structure is called the DataFrame. 
DataFrames allow you to store and manipulate tabular data in rows of observations and columns of variables, much like excel, but with access to all the functionality of python.



Files
-----

- The main Jupyter notebook for the session is found in `Session_2.0.ipynb`
- Images used in the notebooks are stored in the `images` folder
- Example data files (for instance those used in the class tasks) are stored in `files`

