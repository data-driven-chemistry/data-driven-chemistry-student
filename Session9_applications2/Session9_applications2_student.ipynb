{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Session 9: Applications II"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a rel=\"license\" href=\"https://creativecommons.org/licenses/by-sa/4.0/\"><img alt=\"Creative Commons Licence\" style=\"border-width:0\" src=\"https://licensebuttons.net/l/by-sa/4.0/88x31.png\" title='This work is licensed under a Creative Commons Attribution 4.0 International License.' align=\"right\"/></a>\n",
    "\n",
    "Authors: Dr Antonia Mey, J. Jasmin Güven   \n",
    "Email: antonia.mey@ed.ac.uk\n",
    "\n",
    "Thanks to: Dr Rafal Szabla\n",
    "\n",
    "## Learning outcomes\n",
    "1. Practice reading and manipulating 'real' data files and trouble shoot them.\n",
    "2. Understand hidden characters in files.\n",
    "3. Estimate error bars and plot them from absorbance data.\n",
    "4. Analyse UV-Vis data using Beer-Lambert's law.\n",
    "5. Load protein trajectory data from a simulation.\n",
    "6. Compute running averages over trajectories (dataseries)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Jupyter cheat sheet**:\n",
    "- to run the currently highlighted cell, hold <kbd>&#x21E7; Shift</kbd> and press <kbd>&#x23ce; Enter</kbd>;\n",
    "- to get help for a specific function, place the cursor within the function's brackets, hold <kbd>&#x21E7; Shift</kbd>, and press <kbd>&#x21E5; Tab</kbd>;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "\n",
    "\n",
    "\n",
    "\n",
    "### Application 1: UV-vis spectroscopy\n",
    "1. [What is UV-vis spectroscopy again?](#uv_recap)      \n",
    "2. [Estimating and plotting errorbars from data](#errorbars)     \n",
    "3. [How to use Beer Lambert's Law to find unknown concentrations](#beer_lambert)\n",
    "\n",
    "### Application 2: Protein crystallography and simulations\n",
    "4. [What is a protein crystal?](#proteins)  \n",
    "5. [Finding running averages in timeseries data](#running_average)\n",
    "6. [Assessing the stability of a protein in a simulation](#stability)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1. Recap of ultraviolet visible spectroscopy\n",
    "<a id='uv_recap'></a>\n",
    "In this section you will learn how to analyse data you have collected with a UV-vis spectrometer such as [this one](https://www.agilent.com/en/product/molecular-spectroscopy/uv-vis-uv-vis-nir-spectroscopy/uv-vis-uv-vis-nir-systems/cary-60-uv-vis-spectrophotometer). UV-vis spectroscopy is very useful in determining the concentration of UV active compounds. \n",
    "Both transition metals and organic compounds with conjugated $\\pi$ systems will often be active in the UV-visible region of light. \n",
    "<img src=\"images/light.jpg\" alt=\"light\" width=\"400\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1.1 Principles of Colorimetry\n",
    "For example, The color of Allura Red solution is... red! Generally, the observed color is complementary to the color of light absorbed. In Figure 2, red is complementary to green. Thus, Allura Red absorbs primarily wavelengths in the 480-560 nm range. Wavelengths of 640-700 nm are not absorbed but transmitted, thus resulting in our perception of a red solution. \n",
    "\n",
    "<img src=\"images/light2.png\" alt=\"light\" width=\"300\"/>\n",
    "\n",
    "Take a look at the colour wheel of wavelengths of light corresponding to each color for transmission and absorbance. \n",
    "In general the higher the concentration of the compound that is absorbing light, the greater the absoprtion at that frequency. We will be looking at peaks of absorptions measured in the lab for this session. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's put all our imports here as you work through the notebook, be aware some may be missing!\n",
    "import glob\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import sys\n",
    "import os.path\n",
    "sys.path.append(os.path.abspath('../'))\n",
    "from helper_modules.mentimeter import Mentimeter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.2. Meet Rachel\n",
    "\n",
    "Rachel is a final year student and as part of her final year project she needs to estimate the unknown concentration of the dye Rhodamine 6G in methanol, because she forgot to label one of her samples. Luckily she can use UV-Vis spectroscopy of Rhodamine in methanol and use Beer Lambert's law to estimate the concentration of the unlabelled sample. Her data can be found in the folder `data/Section4`. She has never tried this analysis in Python before, so rather than working straight on the actual dataset she wanted to try some parts of her analysis on different sets of data first. Can you help her fix her code and find the concentration of her unlabelled sample? \n",
    "\n",
    "<img src=\"images/Rachel.png\" alt=\"Rachel\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Warm up exercises: Time for the break out room\n",
    "Please attempt exercises:   \n",
    "1.1 Reading in UV spectroscopy data and plot the data.   \n",
    "1.2 Find the peak in the absorbance data. (c.f. Mass Spec data last week)\n",
    "\n",
    "\n",
    "<img src=\"images/breakout-room.png\" alt=\"drawing\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the directory `data/Section1` you find a file called `Rhodamine 6G in methanol .csv` Rachel collected in the lab to measure the absorbance of the dye Rhodamine 6G with respect to wavelength. \n",
    "\n",
    "This is what Rhodamine 6G looks like:\n",
    "<img src=\"images/Rhodamine_6G.png\" alt=\"drawing\" width=\"200\"/>\n",
    "\n",
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 1.1 </b>: Read the data and plot it in a sensible way. (Try different ways, pandas, numpy, or even a completely different way). \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Please give us some idea of errors you encounter with this task in the mentimeter wordcloud. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mentimeter(vote='https://www.menti.com/yb1tryvetf').show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mentimeter(result='https://www.mentimeter.com/s/2c716aba022651cec4d9504870bc50e2/3f48b04777a3').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 1.2</b> : Find the highest absorbance peak in the data file:  'data/Section1/Rhodamine 6G in methanol .csv'. \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 2. Estimating an error on an absorption spectrum\n",
    "<a id='errorbars'></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Rachel has taken 5 repeat measurements of PAO in ethanol at a given concentration, another UV-vis active molecule. She wants to know what the error on the absorbance for this measurement. Rather than plotting the 5 repeats in one plot, she wants a plot of the mean absorbance and the standard deviation of the absorbance as error bars. Can you help her with this? The data can be found in `data/Section2`. Here is an example of all the data in one plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pao_data_files = glob.glob(\"data/Section2/*.csv\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "for d_file in pao_data_files:\n",
    "    data = pd.read_csv(d_file, names=['lam','absorb', 'not_needed'], skiprows=2)\n",
    "    ax.plot(data['lam'], data['absorb'], label=d_file)\n",
    "    ax.legend()\n",
    "ax.set_xlabel('wavelength / nm', fontsize=15)\n",
    "ax.set_ylabel('absorbance / arb. units', fontsize=15)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 2.1</b>: Rachel has started writing code that will help her find the mean of each 5 measures of absorbance at each wavelength, as well as the standard deviation, can you help her fix her code in the cells below? You don't have to use the same way she started to solve the problem if you prefer a different and maybe easier route!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>Hint</b>: Use the print function to help you debug some of her code! Try out the bits that fail. Google errors you get!\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## Rachel's attempt\n",
    "\n",
    "wavelengths_array = []\n",
    "absorbance_array = []\n",
    "\n",
    "# Let's first read the data into an numpy array\n",
    "for d_file in pao_data_files:\n",
    "    data = pd.read_csv(d_file, names=['lam','absorb', 'not_needed'], skiprows=2)\n",
    "    if wavelengths_array is None:\n",
    "        wavelengths_array = data['lam'].tolist()\n",
    "    else:\n",
    "        assert(data['lam'] == wavelengths_array)\n",
    "        print('we already have wavelengths and they match')\n",
    "    absorbance_array.append(data['absorb'].tolist())\n",
    "    \n",
    "# Convert the two lists to numpy arrays\n",
    "wave_lengths = np.array(wavelengths_array)\n",
    "absorbance = np.array(absorbance_array)\n",
    "# Work out the mean and standar deviation of the absorbance array\n",
    "print('Do I need another loop here or can I work out the mean and error differently?')\n",
    "mean_absorbance = None\n",
    "std_absorbance = None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 2.2</b>: Can you show Rachel how to plot the mean and the standard deviation as an errorbar plot? Only plot the region between 200 nm and 400 nm wavelength.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>ADVANCED TASK 3.3</b>: Rather than using error bars on each data point can you use a shaded region to indicate the 1 σ confidence interval instead? Look at the example below. \n",
    "</div>\n",
    "\n",
    "**Hint**: Take a look at the function `fill_between` in matplotlib."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src=\"images/fill-between.png\" alt=\"fill\" width=\"400\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 3. Find the unknown concentration of the compound based on Beer Lambert's Law\n",
    "<a id='beer_lambert'></a>\n",
    "In this section we will put some of the indidividual parts together of the previous sections to help identify Rachel the concentration of the sample series E she measured, but forgot to write down the concentration.\n",
    "\n",
    "You will find her experimental absorbance data in `data/Section3`. For each concentration there are 3 measurements. There is a file called `data/Section3/concentrations.csv`, that tell you which sample had which concentration. For some reason sample `C` has noted down a concentration of `NaN`. You should be able to figure out the concentration at which the sample was measured though based on Beer Lambert's Law. You can use the data you have to create a calibration plot and make sure of the linearity of the Beer Lambert law. It stats that the absorption of light by a substance is proportional to its concentration in solution, or in equation format:\n",
    "\n",
    "$$A = \\epsilon l c,$$\n",
    "\n",
    "where $A$ is the absorbance (unitless), $\\epsilon$ is the molar absorptivity coefficient (M$^{-1}$cm$^{-1}$), $l$ is the pathlength of light through the cuvette (cm), and $c$ is the concentration (M).\n",
    "\n",
    "A typical calibration curve will look like this:\n",
    "<img src=\"images/Calibration_plot.png\" alt=\"calibration\" width=\"400\"/>\n",
    "Image courtesy of Vernier Software and Technology\n",
    "\n",
    "In Rachel's case she has 4 repeat measurements of her absorbance with known concentration and 3 repeats of a measurements with unknown concentration. In this section you will reconstruct what the concentration of Rhodamine 6G was in sample `C`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time for the break out room\n",
    "Please attempt exercises:   \n",
    "3.1 Compute the mean absorbance at all concentrations.  \n",
    "3.2 Find the peak in the absorbance data at around 530 nm.   \n",
    "3.3 Plot the peaks of the absorbance against the known concentrations.   \n",
    "3.4 Use Beer Lambert's Law to fit a line to the data and determine the concentration of `C`.\n",
    "\n",
    "\n",
    "<img src=\"images/breakout-room.png\" alt=\"drawing\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 3.1</b>: Compute the mean absorbance curve of each concentration and plot these.\n",
    "</div>\n",
    "\n",
    "- Write a function called `mean_absorbance` to the easily loop over this. You should be able to reuse what you have done in Section 2 and wrap this into a function.\n",
    "- Loop over all 5 data sets and compute mean absorbance and plot the absorbance v. wavelength (don't worry about error bars this time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# You answer here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#fig, ax = plt.subplots(111)\n",
    "for concentrations in ['A', 'C', 'D', 'E', 'F']:\n",
    "    f_names_c = glob.glob('data/Section3/*'+concentrations+\"*.csv\")\n",
    "    lams, absorbs = mean_absorbance(f_names_c)\n",
    "    plt.plot(lams,absorbs, label=concentrations)\n",
    "    plt.xlim(450,550)\n",
    "    plt.ylim(0.0,0.12)\n",
    "    plt.legend()\n",
    "    plt.xlabel('wavelength / nm')\n",
    "    plt.ylabel('absorbance / arb. units')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 3.2</b>: Find the peak absorbance  data at 530 nm for each concentration.\n",
    "</div>\n",
    "\n",
    "- Fix Rachel's function called `find_peak_absorbance` to find peaks around 530 nm.\n",
    "- Collect the data in a list called `absorbance_peaks`\n",
    "\n",
    "Hint: You can find maxima with either `scipy.signal` or define a wavelength range where you know you only have one peak signal. Use boolean masks to define index ranges of e.g. 500-550 nm ranges to then find a maximum in the absorbance in this range. \n",
    "\n",
    "There are many different ways to solve this problem, so be creative!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n",
    "def find_peak_absorbance(absorbance_data, wave_lengths, idx_of_wavelength_range=None, height=None):\n",
    "    r''' find peak absorbance in a given range of wavelengths\n",
    "    Parameters:\n",
    "    -----------\n",
    "    absorbance_data : array\n",
    "        array of all the whole dataset\n",
    "    \n",
    "    wave_lengths : nd array \n",
    "        array corresponding to the wave_lengths\n",
    "    \n",
    "    idx_of_wavelength_range : \n",
    "        indexes of absorbance data array that correspond to e.g. 500-550 nm wavelength range\n",
    "    \n",
    "    height: float \n",
    "        peak height used by scipy.signal.find_peaks\n",
    "        \n",
    "    Returns:\n",
    "    --------\n",
    "    wave_length_at_max : float\n",
    "        wavelength of the corresponding absorbance maximum\n",
    "    max_absorbance : float\n",
    "        data point we are after\n",
    "    \n",
    "    '''\n",
    "    return wave_length_at_max, max_absorbance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# There are some issues in the code below too can you find them?\n",
    "\n",
    "# Define the list\n",
    "absorbance_peaks = []\n",
    "# loop over concentrations\n",
    "for concentrations in ['A', 'C', 'D', 'E', 'F']:\n",
    "    f_names_c = glob.glob('data/Section3/*'+concentrations+\"*.csv\")\n",
    "    # get the mean absorbance and wavelengths\n",
    "    wave_lengths, absorbance_at_given_c = mean_absorbance(f_names_c)\n",
    "    # find the indexes using a boolean mask\n",
    "    wave_length_idxs = np.where(wave_lengths> 450 & wavelngths < 550)\n",
    "    wavelength_max, absorbance_max = find_peak_absorbance(absorbance_at_given_c, wave_legth, idx_of_wavelength_range=wave_length_idxs, height=0.012)\n",
    "    print(wavelength_max,absorbance_max)\n",
    "    absorbance_peaks.append(absorbance_max)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 3.3</b>: Read in known concentrations and plot concentration against absorbance. \n",
    "</div>\n",
    "\n",
    "- Rachel has made a file with all the concentrations you can find in `data/Section3/concentrations.csv`. This is where you will notice that for `C` the entry is `NaN`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 3.4</b>: Find the concentration of sample C based on Beer Lambert's Law.  \n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your answer\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Break\n",
    "<img src=\"images/break.png\" alt=\"drawing\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 4. Working with protein crystals and molecular simulations\n",
    "<a id=proteins></a>\n",
    "To understand the structure of protein, polymers that are made up of 20 different amino acids. It is possible to crystalise proteins and then use X-ray's to probe their structure. \n",
    "\n",
    "All structural data of proteins are collected in the [protein data bank](https://www.rcsb.org) (PDB). With over 180000 protein structures available. 3-D structure data is invaluable for applications in many different areas of chemistry, as proteins underpin most vital functions in living organisms as natural catalysts. \n",
    "\n",
    "### 4.1 PDB files\n",
    "To store information about proteins a fileformat called pdb was invited. It stores the names of the atoms in the protein, as well as the 3-D coordinates that were determined from the crystal structure. Don't worry you will not need to interact much with these files. In Jupyter we can visualise protein structures from pdb files in different ways. In the following we will use py3Dmol.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!head -n 20 data/Section4/1aki.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As part of her final year project Rachel has been looking at the **hen egg white lysozyme** protein. She has been investigating the structure of it and has also run simulations to look at its stability over time. But she needs some help with analysing the simulations and understanding the data. Let's start by looking at the protein in the notebook."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!pip install py3Dmol"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import py3Dmol\n",
    "view = py3Dmol.view(query='pdb:1aki') # This line will grab the pdb file from the data bank\n",
    "view.setStyle({'cartoon':{'color':'spectrum'}})\n",
    "view.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.2 Measuring properties from crystal structures\n",
    "\n",
    "It is possible to measure different properties from the X-ray structure data, such as `bond lengths`, `angles`, etc. You will try some of this as part of the assessment."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4.3 What are molecular dynamics simulations?\n",
    "Molecular dynamics (MD) simulations are a way to understand how proteins may behave inside cells, by using a computer to model this behaviour. Cells are crowded environments full of proteins and water. Take a look at this movie of a simulation of a bacterial cell. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython import display\n",
    "display.HTML(\"<img src='data/Section4/crowded_cell.mp4'></img>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a molecular dynamics (MD) simulation we model atoms and bonds like balls and springs and generate so called trajectories over time using Newton's Laws of motion. \n",
    "\n",
    "$$\\mathbf{F} = \\frac{d^2\\mathbf{r}_i}{dt^2}$$\n",
    "\n",
    "where $\\mathbf{F}$ is the force on each atom $i$ and $\\mathbf{r}_i$ is the position of atom $i$. The above equation is solved at every time-step over a number of time-steps, to calculate the position and velocity of each atom. \n",
    "\n",
    "To get started with an MD simulation you need some initial coordinates for a protein. These are often taken from the protein data bank. \n",
    "\n",
    "Here you see an example of a short trajectory of a single protein: lysozyme.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from IPython import display\n",
    "display.HTML(\"<img src='data/Section4/lys.gif' width='400'></img>\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 5. Excursion into thermodynamics or how to check equilibration\n",
    "\n",
    "Molecular simulations are often run in similar conditions that mimic a lab environment, namely at **constant temperature**, **constant pressure**, and **constant number of particles** (NPT). To achieve this, the simulation needs to get a chance to equilibrate and some clever algorithmic tricks need to be done to achieve $N$ i.e. number of particles, $T$ temperature, and $p$ pressure to be constant. \n",
    "\n",
    "This can be done with pressure and temperature equilibrations. If you want to know more about the algorithms used in these equilibrations, see https://ftp.gromacs.org/pub/manual/manual-5.0.4.pdf.\n",
    "\n",
    "The temperature equilibration step is called NVT, because the number of atoms $N$, volume $V$ and temperature $T$ are kept constant. Similarly, the pressure equilibration step is called NPT.\n",
    "\n",
    "We will also plot the **running average** to see the equilibration better. The running average, denoted SMA for Simple Moving Average, is defined as \n",
    "\n",
    "$$ \\mathrm{SMA} = \\frac{1}{k} \\sum_{i = n-k+1}^{n} p_i$$\n",
    "\n",
    "where $p$ is a data point, $k$ is the window and $n$ is the total number of data points. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time for the break out room\n",
    "In this breakout room you will assess the equilibration of a trajectory Rachel generated with MD simulations. Help her figure out how to compute a running average and plot this. \n",
    "Please attempt exercises:   \n",
    "\n",
    "5.1 NVT equilibration plot   \n",
    "5.2 Fixing the implementation of the SMA function   \n",
    "5.3 Compute SMA of the temperature and plot everything together.  \n",
    "5.4 Density analysis (advanced)\n",
    "\n",
    "<img src=\"images/breakout-room.png\" alt=\"drawing\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 5.1 </b>: Plotting temperature over time from the MD simulation.\n",
    "</div>\n",
    "\n",
    "The time is in picoseconds (ps) and the data is collected for 100 ps. The temperature is in Kelvin. \n",
    "\n",
    "* Load in a file called `temperature.txt` from the folder `data/Section5`. You can take a look at the file in the folder to see what character is used as the separator. \n",
    "* Plot the temperature against time. \n",
    "* Add axis correct axis labels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your code here:\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 5.2 and TASK 5.3 </b>: Write a function that will compute the running average of an observable, e.g. temperature and plot this together with the original data. \n",
    "</div>\n",
    "\n",
    "Define a function `SMA()` that takes in a list of data, total number of data points and the size of the window. Calculate the cumulative sum and from this calculate the running average, and return it.\n",
    "\n",
    "Take a look at the draft Rachel has made, can you fix the function so that it will work? It is easiest to debug problems with the function if you try and plot the output with the data you are trying to compute the running average for.\n",
    "\n",
    "You could also check visually if this is similar to the running average function you can find in `pandas`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n",
    "\n",
    "def SMA(data, n, window):\n",
    "    '''\n",
    "    Function to calculate the simple moving average. \n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    data: list\n",
    "        list of data \n",
    "    n: int\n",
    "        total number of data points\n",
    "    \n",
    "    window: int\n",
    "        size of the running average window\n",
    "    '''\n",
    "\n",
    "    S_i = 0\n",
    "    cumulative_sum = []\n",
    "    for i in range(n):\n",
    "        cumulative_sum.append(S_i)\n",
    "        S_i = S_i + data[i]\n",
    "    cumulative_sum = np.array(cumulative_sum)    \n",
    "    running_avg = cumulative_sum / float(window)\n",
    "    return running_avge"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 10-ps average for NVT\n",
    "\n",
    "1. Plot the temperature data in black, with label `'Data'`\n",
    "2. Plot the 10-ps running average for temperature in red, with label `'10-ps average'`\n",
    "3. Define axis labels and add a legend."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer for plotting data and running average\n",
    "fig = plt.figure()\n",
    "ax = fig.add_subplot()\n",
    "ax.plot(time,temperature, color = 'black')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mentimeter do you think the simulation is equilibrated? YES? NO? I don't know.\n",
    "Mentimeter(vote=\"https://www.menti.com/y2sihiqcyr\").show()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mentimeter(result=\"https://www.mentimeter.com/s/807a0fe288a596bc3154275f4ed14a2d/33da515773f7\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>ADVANCED TASK 5.4 </b>: NPT equilibration with density \n",
    "</div>\n",
    "\n",
    "The time is in picoseconds (ps) and the data is collected for 100 ps. The density is in kg m$^{-3}$. \n",
    "\n",
    "1. Load in a file called `density.txt` from the folder `data`. You can take a look at the file in the folder to see what character is used as the separator. \n",
    "2. Plot the density against time. \n",
    "3. Plot the SMA of density in red.\n",
    "4. Add correct axis labels and a legend.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your answer\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 6. Assessing the stability of simulations\n",
    "In this part we will consider the root-mean-square deviation (RMSD) and the radius of gyration of the protein during the simulation. These two quantities are useful to judge if the protein maintains it shape, i.e. if the simulation is stable or the protein breaks apart. \n",
    "\n",
    "The root-mean square deviation is given by\n",
    "$$\\mathrm{RMSD} = \\sqrt{\\frac{1}{N} \\sum_{i=0}^{N} (v_i - w_i )^2}$$\n",
    "\n",
    "where $v_i$ is the position of a reference structure and $w_i$ is the structure we're interested in.\n",
    "\n",
    "The radius of gyration is a *measure of the compactness* of a protein, and is given by\n",
    "\n",
    "$$R_\\mathrm{g}(x) = \\sqrt{\\frac{\\sum_i m_i r_i(y)^2 + m_ir_i(z)^2}{\\sum_i m_i}}$$\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time for the break out room\n",
    "In this breakout room you will assess the structural stability of a trajectory Rachel generated with MD simulations. Help her figure out how to compute a running average and plot this. \n",
    "Please attempt exercises:   \n",
    "\n",
    "6.1 Radius of gyration plot   \n",
    "6.2 Average radius of gyration   \n",
    "6.3 RMSD plot (advanced)    \n",
    "\n",
    "\n",
    "<img src=\"images/breakout-room.png\" alt=\"drawing\" width=\"200\"/>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 6.1 </b>: Radius of gyration plot\n",
    "</div>\n",
    "\n",
    "The radius of gyration data is given in the file `data/Section6/gyrate.txt`. The time is in picoseconds (ps) and the radius of gyration values are given in nanometers (nm). \n",
    "\n",
    "1. Load the file gyrate.txt in.\n",
    "2. Plot the radius of gyration. \n",
    "3. Add axis labels.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# your answer here\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<details>\n",
    "<summary> <mark> EXAMPLE SOLUTION:</mark> </summary>\n",
    "    \n",
    "```python\n",
    "\n",
    "print('To Be Implemented')\n",
    "\n",
    "```\n",
    "\n",
    "</details>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-success\">\n",
    "    <b>TASK 6.2 </b>: Average radius of gyration\n",
    "</div>\n",
    "\n",
    "We can calculate the average from the $R_\\mathrm{g}$ data and use the standard deviation as an estimate of the error on the mean value. \n",
    "\n",
    "Code this in the cell below and output the values nicely.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-info\">\n",
    "    <b>ADVANCED TASK 6.3 </b>: RMSD plots\n",
    "</div>\n",
    "\n",
    "As with the above equilibration plots, we want to see if the RMSD plot levels off. \n",
    "\n",
    "To plot the RMSD plots, we have to load in two files: RMSD.txt and RMSD-xtal.txt (both are found in `data/Section6`), where the former is the simulated structure and the latter is the crystal structure. \n",
    "\n",
    "The RMSD is measured in nanometers (nm) and time is now in nanoseconds (ns).\n",
    "\n",
    "1. Load the files in.\n",
    "2. Plot both RMSD and RMSD_xtal in the same plot. \n",
    "3. Add axis labels and a legend.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Your answer here\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Questions for discussion\n",
    "\n",
    "1. Has the protein stabilised in the MD simulation?\n",
    "2. Why aren't the MD structure and the crystal structure always perfectly overlapping?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Mentimeter\n",
    "\n",
    "Mentimeter(vote=\"https://www.menti.com/a4nwnd6xne\").show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mentimeter(result=\"https://www.mentimeter.com/s/03b6d88b44ce9a2e30738cf1e68ed824/e404dd0089be\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Answers\n",
    "\n",
    "1. Yes, you can see it levels off to roughly 0.1 nm.\n",
    "2. This is because the MD structure has been energy-minimised and because the position restraints are not 100% accurate. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Feedback"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Mentimeter(vote=\"https://www.menti.com/jfhzzoi566\").show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## THE END!"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
