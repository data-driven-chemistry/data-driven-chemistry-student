"""
A module to simplify the creation and editing of flowcharts interactively within Jupyter notebooks.
"""

__author__="James Cumby"
__email__="James.cumby@ed.ac.uk"

__all__ = ['Flowchart',]


from pyvis.network import Network
from IPython.display import IFrame as IFrame_orig
from IPython.display import Javascript
from bs4 import BeautifulSoup
import os


class Inline_Network(Network):
    """
    Modified PyVIS network to allow html generation without writing to file.
    
    """
    def make_html(self, notebook=False):
        """
        This method gets the data structures supporting the nodes, edges,
        and options and updates the template to write the HTML holding
        the visualization.
        :type name_html: str
        """
        # here, check if an href is present in the hover data
        use_link_template = False
        for n in self.nodes:
            title = n.get("title", None)
            if title:
                if "href" in title:
                    """
                    this tells the template to override default hover
                    mechanic, as the tooltip would move with the mouse
                    cursor which made interacting with hover data useless.
                    """
                    use_link_template = True
                    break
        if not notebook:
            with open(self.path) as html:
                content = html.read()
            template = Template(content)
        else:
            template = self.template

        nodes, edges, heading, height, width, options = self.get_network_data()

        # check if physics is enabled
        if isinstance(self.options, dict):
            if 'physics' in self.options and 'enabled' in self.options['physics']:
                physics_enabled = self.options['physics']['enabled']
            else:
                physics_enabled = True
        else:
            physics_enabled = self.options.physics.enabled

        self.html = template.render(height=height,
                                    width=width,
                                    nodes=nodes,
                                    edges=edges,
                                    heading=heading,
                                    options=options,
                                    physics_enabled=physics_enabled,
                                    use_DOT=self.use_DOT,
                                    dot_lang=self.dot_lang,
                                    widget=self.widget,
                                    bgcolor=self.bgcolor,
                                    conf=self.conf,
                                    tooltip_link=use_link_template)
    

class Flowchart:
    """
    A wrapper class around PyVis to simplify the creation and immediate editing of flow charts within
    Jupyter notebooks.
    
    Atributes
    ---------
    
    Methods
    -------
    
    """
    
    # Dictionary to hold all Flowchart objects (based on ids) so that they can be accessed again from Javascript.
    global_IDs = {}
    
    def __init__(self,
                 labels,
                 edges = None,
                 shapes = 'box',
                 addEdge = True,
                 addNode = False,
                 template = 'default',
                 node_kwargs = {},
                 ):
        """
        Parameters
        ----------
        labels : list
            List of item labels used to construct the flow chart boxes
        edges : list of tuples, optional
            Pairs of integer-indexed edges to include in the created flowcharts (deault None)
        shapes : list or string, optional
            Shape definitions passed to VisJS (default box). If a single string is passed all
            elements have the same shape, or a list (equal length to labels) can define each individual 
            shape.
        addEdge : boolean, optional
            Allow edges to be added interactively in the created flowchart (default True)
        addNode : boolean, optional
            Allow nodes to be added interactively in the created flowchart (default False)
        template : str, optional
            HTML output template to use for rendering (default pyvis_template.html)
        node_kwargs : dict, optional
            Additional properties to be passed to Network.add_nodes
            
        """
        
        # Set up default shape colours
        self.default_shape_colors = {'box': 'SkyBlue',
                                     'ellipse': 'Pink',
                                     'circle': 'LightGray',
                                    }
           
        assert len(labels) > 0
        
        if template == 'default':
            self.template = os.path.join(os.path.dirname(__file__), 'pyvis_template.html')
        else:
            self.template = os.path.normpath(template)
           
        self.graph = Inline_Network(notebook=True, directed=True)
        
        # Modify physics so labels avoid each other
        #self.graph.barnes_hut(overlap = 1)
        
        self.graph.add_nodes(range(len(labels)), label=labels, **node_kwargs)
        
        if edges is not None:
            self.graph.add_edges(edges)
            
        if isinstance(shapes, str):
            flow_shapes = [shapes]*len(labels)
            colors = [self.default_shape_colors[shapes]]*len(labels)
        elif isinstance(shapes, list):
            assert len(shapes) == len(labels)
            flow_shapes = shapes
            colors = [self.default_shape_colors[i] for i in flow_shapes]
        else:
            raise ValueError('shapes should either be a string or list of strings')

        for i, n in enumerate(self.graph.nodes):
            n['shape'] = flow_shapes[i]
            n['color'] = colors[i]
            
        # Set default options for PyVis 
        self.graph.set_options("""{"manipulation":{"initiallyActive":true, 
                                  "addNode":false,
                                  "addEdge":true}
                                  }""")
                                  
                         
                         
        # Empty list to hold future iframes
        self.iframes = []
        
        self.global_IDs[id(self)] = self
        
        self.positions = ""
        
        
    def show_inline(self):
        """
        Prepare javascript using custom HTML template, and return the javascript display.
        
        Intended to produce pure HTML for appending to Jupyter output, without needing an 
        IFrame. This allows the code to interact with the Javascript execution, passing variables
        as needed.
        
        Parameters
        ----------
        custom_template : str, optional
            PyVIS HTML template to use, the <script> part of which is extracted (default pyvis_template.html).
        """
        
        self.graph.prep_notebook(custom_template = True, custom_template_path = self.template)
        self.graph.make_html(notebook=True)
        
        HTML_soup = BeautifulSoup(self.graph.html, 'html.parser')
        
        # Set up javascript with additional parts
        self.jscript = """
            require.config({
              paths: {
                  vis: 'https://cdnjs.cloudflare.com/ajax/libs/vis/4.16.1/vis-network.min',
              },
            });

            element.append("<div id='CHARTID' style='width: 400px; height: 400px; border: 1px solid lightgray; position: relative;'></div>");

            require(['vis'], function (vis) {
                JAVASCRIPT_HERE
                
            });
            """.replace('JAVASCRIPT_HERE', HTML_soup.body.script.contents[0]).replace('CHARTID', str(id(self)))
            
        return Javascript(self.jscript)

     
    def show_iframe(self, outfile):
        """
        Prepare html using custom HTML template, and then show.
        
        Intended to replicate the behaviour of PyVIS - an HTML file is written to
        disk, and then loaded into an iframe within Jupyter (safe, but prevents interaction
        with the Javascript execution)
        
        Parameters
        ----------
        outfile : str
            Name of HTML file to write static output.
        custom_template : str, optional
            PyVIS HTML template to use (default 'pyvis_template.html') 
        """

        self.graph.prep_notebook(custom_template = True, custom_template_path = self.template)
        self.iframes.append(self.graph.show(outfile))
        
        return self.iframes[-1]
        
        
class IFrame(IFrame_orig):
    """
    Modified IPython IFrame to allow the use of srcdoc
    """
    
    iframe = """
        <iframe
            width="{width}"
            height="{height}"
            src="{src}{params}"
            frameborder="0"
            allowfullscreen
        ></iframe>
        """
    
    def __init__(self, src, width, height, **kwargs):
        self.src = src
        self.width = width
        self.height = height
        self.params = kwargs  

        if self.params.get('srcdoc', False):
            iframe = """
                <iframe
                    width="{width}"
                    height="{height}"
                    srcdoc="{src}"
                    frameborder="0"
                    allowfullscreen
                ></iframe>
                """